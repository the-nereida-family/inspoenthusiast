const config = {
  isProduction: import.meta.env.MODE === 'production'
};
export default config;
