import config from './config';
// eslint-disable-next-line @typescript-eslint/no-explicit-any
const fullPath = (document.currentScript as any)?.src ?? '';
export const DISTRIBUTION_URL = fullPath.substring(0, fullPath.lastIndexOf('/'));
const getAssetUrl = (path: string) => `${DISTRIBUTION_URL}/${config.isProduction ? '' : 'public/'}assets/${path}`;

export default getAssetUrl;
