import { RouterProvider, createBrowserRouter } from 'react-router-dom';
import './globals.scss';
import About from './pages/About.tsx';
import Blog from './pages/Blog.tsx';
import Edit from './pages/Edit.tsx';
import BlogDetail from './pages/BlogDetail.tsx';

const App = () => {
  const router = createBrowserRouter([
    {
      path: '/',
      element: <Blog />
    },
    {
      path: 'blog',
      element: <Blog />
    },
    {
      path: '/blog/:blogId',
      element: <BlogDetail />
    },
    {
      path: 'about',
      element: <About />
    },
    {
      path: 'edit',
      element: <Edit />
    }
  ]);

  return <RouterProvider router={router} />;
};

export default App;
