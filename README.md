# CV showcase

### ⚡ **Project overview:**

- Web application to host a blog with Amazon afiliate program links
- For no will be just static content but in future AMAZON API is needed
  <br />

### ⚙ **Requirements and setup:**

- [node and npm](https://nodejs.org/)
- [git](https://git-scm.com/)
- Set your name in git `git config --global user.name "your name"`
- Set your email in git `git config --global user.email "your email"` (verify with `cat .git/config`)
- We use prettier formatter so set it on save in your IDE as default formatter and install any additional prettier extension
- After cloning the repo, run `npm install`
- Environment variables are needed to provide firebase credentials, request them from a dev.
- Run entire app: `npm run dev`
- Build entire app: `npm run build`
- Serve entire build : `npm run preview`

<br />

### 📝 **Code conventions:**

- No debugging code in PR
- No comments in PR
- Make sure your changes are responsive and they not brake the desktop/mobile view
- We follow [Atomic Design](https://atomicdesign.bradfrost.com/table-of-contents/) for our folder/files structures (except templates)
- We follow some sort of [BEM (Blocks, Elemets, Modifiers)](https://getbem.com/introduction/) css methodology, check the current classNames in an existing module.

<br />

### 🔀 **Branches and pull requests**:

- All branches are created from the latest `main` branch
- All branches are merged into `main` branch
- Features Branch name must start with `feature/INSPO-<ticketNumber>-<kebab-case-name>`
- Bugs Branch name must start with `bugfix/INSPO-<ticketNumber>-<kebab-case-name>`
- PR name must start with `INSPO-<ticketNumber> <Name with spaces>`
- Commits must start with `INSPO-<ticketNumber> <Descriptive message>`
  <br />

### 🚀 **Deploys**:

- to be added, we will define between gitlab pages and render
